﻿

namespace Cart.Domain.Dtos
{
   public class ProductDto
    {
        public Guid Id { get; set; }
        public decimal Price { get; set; }
        public int Stock { get; set; }
        public StoreStatusEnum Status { get; set; }

    }
}
