﻿

namespace Cart.Domain.EvenEnvelopes.Cart
{
    public record CartStatusChanged(
        Guid Id,
        Guid CustomerId,
        List<CartProductItem> CartProducts,
        CartStatusEnum Status)
    {
        public static CartStatusChanged Create(
            Guid id,
            Guid customerId,
            List<CartProductItem> cartproducts,
            CartStatusEnum status
            ) => new(id, customerId, cartproducts, status);
    }



    public class CartProductItem
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public int Quantity { get; set; }
    }
}
