﻿using Cart.Domain.Dtos;
using Cart.Domain.Services;
using HotChocolate.Authorization;

namespace Cart.GraphQL.Scema.Queries
{
    [ExtendObjectType("Query")]
    public class CartProductQuery
    {
        private readonly ICartProductService _services;
        public CartProductQuery(ICartProductService services)
        {
            _services = services;
        }
        [Authorize(Roles = new[] { "customer" })]
        public async Task<IEnumerable<CartProductDto>> GetallCartProductAsync()
        {
            IEnumerable<CartProductDto> result = await _services.All();
            return result;
        }
        [Authorize(Roles = new[] { "customer" })]
        public async Task<CartProductDto> GetCartProductById(Guid id)
        {
            return await _services.GetCartProductById(id);
        }
    }
}
