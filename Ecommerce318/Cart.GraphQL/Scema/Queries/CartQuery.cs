﻿using Cart.Domain.Dtos;
using Cart.Domain.Entities;
using Cart.Domain.Services;


namespace Cart.GraphQL.Scema.Query
{
    [ExtendObjectType("Query")]
    public class CartQuery
    {
        private readonly ICartService _services;
        public CartQuery(ICartService service)
        {
            _services = service;
        }

        //[Authorize(Roles = new[] { "customer" })]
        public async Task<IEnumerable<CartDto>> GetAllCartsAsync()
        {
            IEnumerable<CartDto> result = await _services.All();
            return result;
        }
        //[Authorize(Roles = new[] { "customer" })]
        public async Task<CartDto> GetCartByIdAsync(Guid id)
        {
            return await _services.GetCartById(id);
        }

        public async Task<IEnumerable<CartsEntity>> GetAllInAsync()
        {
            return await _services.AllIn();
        }
    }
}
