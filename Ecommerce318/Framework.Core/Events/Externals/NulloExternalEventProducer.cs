﻿

namespace Framework.Core.Events.Externals
{
    public interface NulloExternalEventProducer : IExternalEventProducer
    {
        public Task Publish(IEventEnvelope @event, CancellationToken ct)
        {
            return Task.CompletedTask;
        }
    }
}
