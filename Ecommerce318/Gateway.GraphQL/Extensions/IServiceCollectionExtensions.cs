﻿

namespace Gateway.GraphQL.Extensions
{
    public static class IServiceCollectionExtensions
    {
        public const string LookUps = "LookUpService";
        public const string Stores = "StoreService";
        public const string Users = "UserService";
        public const string Carts = "CartService";

        public static IServiceCollection AddHttpClientServices(this IServiceCollection services)
        {
            services.AddHttpClient(LookUps, l => l.BaseAddress = new Uri("https://localhost:7128/graphql"));
            services.AddHttpClient(Stores, s => s.BaseAddress = new Uri("https://localhost:7075/graphql"));
            services.AddHttpClient(Users, u => u.BaseAddress = new Uri("https://localhost:7172/graphql"));
            services.AddHttpClient(Carts, c => c.BaseAddress = new Uri("https://localhost:7122/graphql"));

            services
                .AddGraphQLServer()
                .AddRemoteSchema(LookUps)
                .AddRemoteSchema(Stores)
                .AddRemoteSchema(Users)
                .AddRemoteSchema(Carts);

            return services;
        }
    }
}
