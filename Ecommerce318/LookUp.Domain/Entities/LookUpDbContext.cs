﻿
using LookUp.Domain.Entities.Configuration;
using Microsoft.EntityFrameworkCore;

namespace LookUp.Domain.Entities
{
    public class LookUpDbContext : DbContext
    {
        public LookUpDbContext(DbContextOptions<LookUpDbContext> options) : base(options)
        {

        }

        public DbSet<AttributeEntity> Attributes { get; set; }
        public DbSet<CurrenciesEntity> Currencies { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new AttributeConfiguration());
            modelBuilder.ApplyConfiguration(new CurrenciesConfiguration());
        }


    }
}
