﻿

namespace LookUp.Domain.EventEnvelope.Attribute
{
    public record AttributeUpdate(
        Guid Id,
        AttributeTypeEnum Type,
        string Unit
        )
    {
        public static AttributeUpdate UpdateData(
            Guid id,
            AttributeTypeEnum type,
            string unit
            ) => new(id, type, unit);
    }
}
