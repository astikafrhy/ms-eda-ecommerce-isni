﻿
using LookUp.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace LookUp.Domain
{
    public static class ServiceExtention
    {
        public static void AddDomainContext(this IServiceCollection services,
            Action<DbContextOptionsBuilder> optionAction,
            ServiceLifetime contextLifetime = ServiceLifetime.Scoped,
            ServiceLifetime optionLifetime = ServiceLifetime.Scoped)
        {
            services.AddDbContext<LookUpDbContext>(optionAction, contextLifetime,
                optionLifetime);
        }
    }
}
