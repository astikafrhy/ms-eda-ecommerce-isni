﻿using HotChocolate.Authorization;
using LookUp.Domain;
using LookUp.Domain.Dtos;
using LookUp.Domain.Services;

namespace LookUp.GraphQL.Scema.Mutations
{
    [ExtendObjectType("Mutation")]
    public class AttributeMutation
    {
        private readonly IAttributeService _service;

        public AttributeMutation(IAttributeService service)
        {
            _service = service;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<AttributeDto> AddAttributeAsync(AttributeTypeInput attribute)
        {
            AttributeDto dto = new AttributeDto();
            dto.Unit = attribute.Unit;
            dto.Type = attribute.Type;
            var result = await _service.AddAttribute(dto);
            return result;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<AttributeDto> EditAttributeAsync(Guid id, AttributeTypeInput attribute)
        {
            AttributeDto dto = new AttributeDto();
            dto.Id = id;
            dto.Unit = attribute.Unit;
            dto.Type = attribute.Type;
            var result = await _service.UpdateAttribute(dto);
            if (!result)
            {
                throw new GraphQLException(new Error("Attribute not found, 404"));
            }
            return dto;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<AttributeDto> EditStatusAsync(Guid id, LookUpStatusEnum status)
        {
            var result = await _service.UpdateStatus(id, status);

            if (!result)
            {
                throw new GraphQLException(new Error("Attribute not found, 404"));
            }

            return await _service.GetAttributeById(id);
        }

    }
}
