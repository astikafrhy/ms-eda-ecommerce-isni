﻿using HotChocolate.Authorization;
using LookUp.Domain.Dtos;
using LookUp.Domain.Services;

namespace LookUp.GraphQL.Scema.Queries
{
    [ExtendObjectType(Name = "Query")]
    public class CurrenciesQuery
    {
        private readonly ICurrenciesService _service;

        public CurrenciesQuery(ICurrenciesService service)
        {
            _service = service;
        }

        [Authorize]
        public async Task<IEnumerable<CurrenciesDto>> GetAllCurrenciesAsync()
        {
            IEnumerable<CurrenciesDto> result = await _service.All();
            return result;
        }

        [Authorize]
        public async Task<CurrenciesDto> GetCurrenciesByIdAsync(Guid id)
        {
            CurrenciesDto result = await _service.GetCurrenciesById(id);
            return result;
        }
    }
}
