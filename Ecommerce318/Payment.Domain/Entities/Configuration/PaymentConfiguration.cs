﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Payment.Domain.Entities.Configuration
{
    
    public class CartConfiguration : IEntityTypeConfiguration<CartEntity>
    {
        public void Configure(EntityTypeBuilder<CartEntity> builder)
        {
            builder.ToTable("Carts");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.CustomerId).IsRequired();
            builder.Property(x => x.PaymentStatus).IsRequired();
            builder.Property(x => x.Status).IsRequired();
        }
    }

    public class UserConfiguration: IEntityTypeConfiguration<UserEntity>
    {
        public void Configure(EntityTypeBuilder<UserEntity> builder)
        {
            builder.ToTable("Users");
            builder.HasKey(u => u.Id);
            builder.Property(u => u.FirstName).HasMaxLength(50).IsRequired();
            builder.Property(u => u.LastName).HasMaxLength(50).IsRequired();
            builder.Property(u => u.Email).HasMaxLength(50).IsRequired();
        }
    }

    public class ProductConfiguration : IEntityTypeConfiguration<ProductEntity>
    {
        public void Configure(EntityTypeBuilder<ProductEntity> builder)
        {
            builder.ToTable("Products");
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Price).HasPrecision(18, 2).IsRequired();
            builder.Property(p => p.Stock).IsRequired();
            builder.Property(p => p.Status).IsRequired();
        }
    }

    public class CartProductConfiguration : IEntityTypeConfiguration<CartProductEntity>
    {
        public void Configure(EntityTypeBuilder<CartProductEntity> builder)
        {
            builder.ToTable("CartProducts");
            builder.HasKey(cp => cp.Id);
            builder.Property(cp => cp.CartId).IsRequired();
            builder.Property(cp => cp.ProductId).IsRequired();
            builder.Property(cp => cp.Quantity).IsRequired();
        }
    }

}
