﻿

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Payment.Domain.Entities.Configuration;

namespace Payment.Domain.Entities
{
    public class PaymentDbContext : DbContext
    {
        public PaymentDbContext(DbContextOptions<PaymentDbContext> options) : base(options)
        {

        }

        public DbSet<CartEntity> Carts { get; set; }
        public DbSet<CartProductEntity> CartProducts { get; set; }
        public DbSet<UserEntity> Users { get; set; }
        public DbSet<ProductEntity> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CartConfiguration());
            modelBuilder.ApplyConfiguration(new CartProductConfiguration());
            modelBuilder.ApplyConfiguration(new ProductConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());

        }

        public static DbContextOptions<PaymentDbContext> OnConfigure()
        {
            var optionBuilder = new DbContextOptionsBuilder<PaymentDbContext>();
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional : true, reloadOnChange: true);
            optionBuilder
                .UseSqlServer(builder
                        .Build()
                        .GetSection("ConnectionStrings")
                        .GetSection("Payment_Db_Conn").Value);

            return optionBuilder.Options;


        }


    }
}
