﻿

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Payment.Domain.Entities;

namespace Payment.Domain
{
    public static class ServiceExtension
    {
        public static void AddDomainContext(this IServiceCollection services,
            Action<DbContextOptionsBuilder> optionAction,
            ServiceLifetime contextLifetime = ServiceLifetime.Scoped,
            ServiceLifetime optionLifetime = ServiceLifetime.Scoped)
        {
            services.AddDbContext<PaymentDbContext>(optionAction, contextLifetime,
                optionLifetime);
        }
    }
}
