﻿using AutoMapper;
using Framework.Core.Events;
using Framework.Core.Events.Externals;
using Payment.Domain.Dtos;
using Payment.Domain.Entities;
using Payment.Domain.EventEnvelope;
using Payment.Domain.Repositories;


namespace Payment.Domain.Services
{
    public interface IPaymentService
    {
        Task<IEnumerable<CartDto>> All();
        Task<CartDto> Paying (Guid cartId, decimal amount);
    }

    public class PaymentService : IPaymentService
    {
        private readonly ICartRepository _cartRepository;
        private readonly ICartProductRepository _cartProductRrepository;
        private readonly IProductRepository _productRepository;
        private readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;

        public PaymentService(ICartRepository cartRepository, ICartProductRepository cartProductRrepository,
                               IProductRepository productRepository, IMapper mapper, IExternalEventProducer externalEventProducer)
        {
            _cartProductRrepository = cartProductRrepository;
            _cartRepository = cartRepository;
            _productRepository = productRepository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
        }

        

        public async Task<IEnumerable<CartDto>> All()
        {
            return _mapper.Map<IEnumerable<CartDto>>(await _cartRepository.All());
        }

        public async Task<CartDto> Paying(Guid cartId, decimal amount)
        {
            CartDto dto = new CartDto();
            CartEntity entity = await _cartRepository.GetById(cartId);

            if(entity != null)
            {
                if(entity.Total <= amount)
                {
                    entity.Pay = amount;
                    entity.Status = CartStatusEnum.Paid;
                    entity.PaymentStatus = PaymentStatusEnum.Paid;
                    await _cartRepository.Update(entity);
                    var list = await _cartProductRrepository.GetCartById(cartId);

                    //checking stock
                    foreach(var item in list)
                    {
                        var product = await _productRepository.GetById(item.ProductId);
                        if (product != null)
                        {
                            if (product.Stock < item.Quantity)
                                return null;
                        }
                        else
                            return null;
                    }


                    List<CartProductItem> cartProducts = _mapper.Map<IEnumerable<CartProductItem>>(list).ToList();

                    //Update stock
                    foreach (var item in list)
                    {
                        var product = await _productRepository.GetById(item.ProductId);
                        if (product != null)
                        {
                            product.Stock = product.Stock - item.Quantity;

                            var selected = cartProducts.Where(o => o.ProductId == item.ProductId).FirstOrDefault();

                            if (selected != null)
                                selected.Stock = product.Stock;

                            await _productRepository.Update(product);
                        }
                        else
                        {
                            return null;
                        }
                    }

                    //var res = await _productRepository.SaveChangesAsync();
                    var result = await _cartRepository.SaveChangesAsync();

                    if (result > 0)
                    {
                        var externalEvent = new EventEnvelope<PaymentCreated>(
                            PaymentCreated.Create(
                                entity.Id,
                                cartProducts,
                                entity.Status
                            )
                        );

                        await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                        return _mapper.Map<CartDto>(entity);
                    }
                }

                return null;
            }
            return null; 
        }
    }
}
