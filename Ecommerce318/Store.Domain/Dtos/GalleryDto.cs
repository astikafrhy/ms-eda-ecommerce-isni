﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain.Dtos
{
    public class GalleryDto
    {
        public Guid Id { get; set; }
        public string FileLink { get; set; }
        public string Name { get; set; } 
        public string Description { get; set; } 
        public StoreStatusEnum Status { get; set; }
    }
}
