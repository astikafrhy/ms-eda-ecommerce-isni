﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain.EventEnvelope.Product
{
    public record ProductUpdateStatus(
        Guid Id,
        StoreStatusEnum Status
        )
    {
        public static ProductUpdateStatus UpdateStatus
            (
                Guid id,
                StoreStatusEnum status
            ) => new(id, status);        
    }
}
