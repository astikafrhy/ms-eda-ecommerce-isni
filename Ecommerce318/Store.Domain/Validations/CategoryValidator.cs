﻿

using FluentValidation;
using Store.Domain.Dtos;

namespace Store.Domain.Validations
{
    public class CategoryValidator : AbstractValidator<CategoriesDto>
    {
        public CategoryValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Name cannot empty");
            RuleFor(x => x.Description).MinimumLength(5).WithMessage("Description minimum 5 chars");
        }

    }
}
