﻿using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Store.Domain;
using Store.Domain.Dtos;
using Store.Domain.Services;

namespace Store.OpenApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly ICategoriesService _service;
        private readonly ILogger<CategoriesController> _logger;
        private IValidator<CategoriesDto> _validator;

        public CategoriesController(ICategoriesService service, ILogger<CategoriesController> logger, IValidator<CategoriesDto> validator)
        {
            _service = service;
            _logger = logger;
            _validator = validator;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _service.All());
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CategoriesDto payload, CancellationToken cancellationToken)
        {
            try
            {
                ValidationResult result = await _validator.ValidateAsync(payload);
                if (!result.IsValid)
                    return BadRequest(result);

                var dto = await _service.AddCategories(payload);
                if (dto != null)
                    return Ok(dto);
            }
            catch (OperationCanceledException ex)
                when (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return BadRequest();
        }

        [HttpPut]
        public async Task<IActionResult> Put(Guid id, [FromBody] CategoriesDto payload, CancellationToken cancellationToken)
        {
            try
            {


                if(id != payload.Id) return BadRequest();

                ValidationResult result = await _validator.ValidateAsync(payload);
                if (!result.IsValid) return BadRequest(result);

                if (payload != null)
                {


                    payload.Id = id;
                    var isUpdated = await _service.UpdateCategories(payload);

                    if (isUpdated)
                        return Ok(isUpdated);
                }
            }
            catch (OperationCanceledException ex)
                when (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }


        [HttpPut("Status")]
        public async Task<IActionResult> Put(Guid id, StoreStatusEnum status, CancellationToken cancellationToken)
        {
            try
            {
                var isUpdated = await _service.UpdateStatus(id, status);

                if (isUpdated)
                    return Ok(isUpdated);
            }
            catch (OperationCanceledException ex)
                when (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }

    }
}
