using Framework.Core.Events;
using Framework.Kafka;
using Microsoft.EntityFrameworkCore;
using Store.Domain;
using Store.Domain.Entities;

var builder = WebApplication.CreateBuilder(args);


builder.Services.AddDbContext<StoreDbContext>(options =>
{
    options.UseSqlServer(builder.Configuration.GetConnectionString("Store_Db_Conn"));
});
// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddStore();
builder.Services.AddEventBus();
builder.Services.AddKafkaProducerAndConsumer();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
