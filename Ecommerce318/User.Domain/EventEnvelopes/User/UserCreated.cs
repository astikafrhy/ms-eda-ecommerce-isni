﻿

namespace User.Domain.EventEnvelopes.User
{
    public record UserCreated
        (
            Guid Id,
            string FirstName,
            string LastName,
            string Email
        )
    {
        public static UserCreated Create
            (
                Guid id,
                string firstName,
                string lastName,
                string email
            ) => new(id, firstName, lastName, email);
    }
}
