﻿using AutoMapper;
using User.Domain.Dtos;
using User.Domain.Entities;

namespace User.Domain.MapProfile
{
    public class EntityToDtoProfile : Profile
    {
        public EntityToDtoProfile() : base("Entity to Dto Profile")
        {
            CreateMap<UsersEntity, UserDto>().ReverseMap();
            CreateMap<UsersEntity, LoginDto>().ReverseMap();
        }

    }
}
