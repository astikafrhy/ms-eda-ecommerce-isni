﻿
using User.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Framework.Auth;

namespace User.Domain.Repositories
{
    public interface IUserRepository
    {
        Task<int> GetCount();
        Task<IEnumerable<UsersEntity>> GetAll();
        Task<IEnumerable<UsersEntity>> GetPaged(int page, int size);
        Task<UsersEntity> GetById(Guid id);
        Task<UsersEntity> Add(UsersEntity entity);
        Task<UsersEntity> Update(UsersEntity entity);
        void Delete(UsersEntity entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
        Task<UsersEntity> Login(string userName, string password);
    }

    public class UserRepository : IUserRepository
    {
        protected readonly UserDbContext _context;
        public UserRepository(UserDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }
        public async Task<UsersEntity> Add(UsersEntity entity)
        {
            _context.Set<UsersEntity>().Add(entity);
            return entity;
        }

        public void Delete(UsersEntity entity)
        {
            _context.Set<UsersEntity>().Update(entity);
        }

        public async Task<IEnumerable<UsersEntity>> GetAll()
        {
            return await _context.Set<UsersEntity>().Where(u => u.Status != UserStatusEnum.Removed).ToListAsync();
        }

        public async Task<UsersEntity> GetById(Guid id)
        {
            return await _context.Set<UsersEntity>().FindAsync(id);
        }

        public Task<int> GetCount()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<UsersEntity>> GetPaged(int page, int size)
        {
            throw new NotImplementedException();
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task<UsersEntity> Update(UsersEntity entity)
        {
            _context.Set<UsersEntity>().Update(entity);
            return entity;
        }
        public virtual void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        public async Task<UsersEntity> Login(string userName, string password)
        {
            return await _context.Set<UsersEntity>()
                .FirstOrDefaultAsync(o => o.UserName == userName && o.Password == Encryption.HashSha256(password));
        }
    }
}
