﻿using User.Domain;
using User.Domain.Dtos;
using User.Domain.Services;

namespace User.GraphQL.Scema.Mutations
{
    [ExtendObjectType("Mutation")]
    public class UserMutation
    {
        private readonly IUserService _service;

        public UserMutation(IUserService service)
        {
            _service = service;
        }

        public async Task<UserDto> AddUserAsync(UserTypeInput inputUser)
        {
            UserDto dto = new UserDto();
            dto.UserName = inputUser.UserName;
            dto.Password = inputUser.Password;
            dto.FirstName = inputUser.FirstName;
            dto.LastName = inputUser.LastName;
            dto.Email = inputUser.Email;
            dto.Type = inputUser.Type;
            var result = await _service.AddUser(dto);
            return result;
        }

        public async Task<UserDto> EditUserAsync(Guid id, UserTypeInput inputUser)
        {
            UserDto dto = new UserDto();
            dto.Id = id;
            dto.UserName = inputUser.UserName;
            dto.Password = inputUser.Password;
            dto.FirstName = inputUser.FirstName;
            dto.LastName= inputUser.LastName;
            dto.Email= inputUser.Email;
            dto.Type= inputUser.Type;
            var result = await _service.UpdateUser(dto);
            if (!result)
                throw new GraphQLException(new Error("User not found, 404"));

            return dto;
        }

        public async Task<UserDto> EditStatusUserAsync(Guid id, UserStatusEnum status)
        {
            var result = await _service.UpdateStatus(id, status);
            if (!result)
                throw new GraphQLException(new Error("User not found, 404"));

            return await _service.GetUserById(id);
        }
    }
}
